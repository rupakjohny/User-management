import React from 'react';
import './App.css';
import Header from './components/Header';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import User from './pages/User';
import Adduser from './pages/Adduser';
import Updateuser from './pages/Updateuser';

function App() {
  return (
    
    
    <>
      <Header />
      <Router>
        <Navbar />
        <Routes>
          <Route exact path='/pages/home' element={<Home />} />
          <Route exact path='/pages/User' element={<User />} />
          <Route exact path='/Adduser' element={<Adduser/>}/>
          <Route exact path='/Updateuser' element={<Updateuser/>}/>
        </Routes>
      </Router>
    </>
  );
}

export default App;
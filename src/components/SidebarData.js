import React from 'react';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [
  {
    title: 'Home',
    path: '/pages/Home',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'User Management',
    path: '/pages/User',
    icon: <IoIcons.IoMdPeople /> ,
    cName: 'nav-text'
  },
 
];
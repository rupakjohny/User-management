import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './Adduser.css';
import { useLocation } from 'react-router';

const UpdateUser = () => {
     const values = {
    
        name: "",
        email: "",
        mobile: "",
        
    };
    const { state } = useLocation();
    const [initialState, setState] = useState(values);
    const [formErrors, setFormErrors] = useState({});

    const { name, email, mobile } = initialState;

    const loadUser=async()=>{
        const response = await fetch('https://react-user-732ec-default-rtdb.firebaseio.com/userrecords/'+state.key+'.json');
        const data = await response.json();
        console.log('Data', data);
        setState(data)
    }

    useEffect(() => {
        loadUser();
        
      }, []);

    const handleInputChange = (e) => {
        let { name, value } = e.target;
        setState({
            ...initialState,
            [name]: value,
        });
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        validate(initialState);
    };

    

    const validate = (values) => {
        const errors = {};
        const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const reg = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
        if (!values.name) {
            errors.name = "Username is required!";
        }
        if (!values.email) {
            errors.email = "Email is required!";
        } else if (!regex.test(values.email)) {
            errors.email = "This is not a valid email!";
        }
        if (!values.mobile) {
            errors.mobile = "Contact is required!";
        } else if (!reg.test(values.mobile)) {
            errors.mobile = "This is not a valid Number!";
        }
        
        console.log("Errors", errors)
        console.log(Object.keys(errors).length)
        setFormErrors(errors)
        if (Object.keys(errors).length === 0) {
            const res = fetch('https://react-user-732ec-default-rtdb.firebaseio.com/userrecords/'+state.key+'.json',
                {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application.json',
                    },
                    body: JSON.stringify({
                        name, email, mobile, 
                    }),

                }

            );
            if (res) {
                setState({
                    name: "",
                    email: "",
                    mobile: "",
                   
                })
            
        }
    }
    }

    return (
        <div className='container mt-5'>
            <div className='row'>
                <div className='col-md-2'></div>
                <div className='col-md-10'>
                    <pre>{JSON.stringify(initialState, undefined, 2)}</pre>
                    <form className="form-horizontal">
                        <div className="row">
                            <div className="col-md-10">
                                <h2>Update User</h2>
                            </div>
                            <div className="col-md-2">
                                <button className="btn btn-danger addbtn">
                                    <Link to='/pages/User'>
                                        Back</Link></button>
                            </div>
                        </div>

                        <div className="row form-group">
                            <div className="col-md-6">
                                <label className='bmd-label-floating'>Name</label>
                            </div>
                            <div className="col-md-6"><input
                                type='text'
                                className='form-control'
                                value={name}
                                name='name'
                                onChange={handleInputChange}
                            /></div>
                            <p>{formErrors.name}</p>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <label className='bmd-label-floating'>Email</label>
                            </div>
                            <div className="col-md-6"><input
                                type='text'
                                className='form-control'
                                value={email}
                                name='email'
                                onChange={handleInputChange}
                            /></div>
                            <p>{formErrors.email}</p>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <label className='bmd-label-floating'>Phone</label>
                            </div>
                            <div className="col-md-6"><input
                                type='text'
                                className='form-control'
                                value={mobile}
                                name='mobile'
                                onChange={handleInputChange}
                            /></div>
                            <p>{formErrors.mobile}</p>
                        </div>
                     
                        <div className="row">
                            <div className="col-md-5">
                            </div>
                            <div className="col-md-7">
                                <button className='btn btn-success btn-raised' onClick={handleSubmit}>Update</button>
                                <button className='btn btn-default'><Link to="/pages/User">Cancel</Link></button>
                            </div>
                        </div>


                    </form>
                </div>
            </div >
        </div >
    )
};

export default UpdateUser;
import React, { Fragment, useState, useEffect } from 'react';
import './User.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';





function User() {
  const [Users, setUserdata] = useState([]);
  const [showUser, setshowUser] = useState([]);
  const navigate = useNavigate();



  async function fetchData() {
    const response = await fetch('https://react-user-732ec-default-rtdb.firebaseio.com/userrecords.json');
    const data = await response.json();
    console.log('Data', data);
    const loadedUser = [];
    if (data !== null) {
      for (const key in data) {
        loadedUser.push({
          key: key,
          name: data[key].name,
          email: data[key].email,
          mobile: data[key].mobile,
        });

        setshowUser(loadedUser);
        console.log(Users);
        let transferedData=[];
         transferedData = showUser.map((userData) =>({
           key:userData.key,
          name : userData.name,
          email : userData.email,
          mobile : userData.mobile,
         })
         )
        setUserdata(transferedData)
        }

    } else {
      setUserdata([]);
    }

  }

  useEffect(() => {
    fetchData();
    console.log("Heyy")
  }, [showUser]);

  async function onDelete(key) {

    await fetch('https://react-user-732ec-default-rtdb.firebaseio.com/userrecords/' + key + '.json', {
      method: 'DELETE',
    })
    fetchData();

  };

  return (
    <Fragment>
      <div className="user-top container">
        <div className="col-md-10">
          <h2>User Management</h2>
        </div>
        <div className="col-md-2">
          <button className="btn btn-danger addbtn">
            <Link to='/Adduser'>
              Add User</Link></button>

        </div>
      </div>
      <div className='container'>
        <table className='table table-stripped'>
          <thead>
            <tr>

              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {Users.map((user) => (
              <tr>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.mobile}</td>
                <td>
                  <button className='btn btn-danger' onClick={() => {
                    navigate('/Updateuser',
                      {
                        state:
                          { key: user.key }
                      }
                    )
                  }
                  }>
                    Update</button>
                  <button onClick={() => onDelete(user.key)}>Delete</button></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>


    </Fragment>
  );
}

export default User;